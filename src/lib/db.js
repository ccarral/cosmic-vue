
const { Pool } = require('pg');
const  { credentials , env }  = require('../../config');
const { debug } = require('./debug');

const { connectionString } = credentials.postgres;

const pool = new Pool({
  connectionString
});

exports.dbQuery = (text,params,callback) =>{
  return pool.query(text,params,callback);
};
