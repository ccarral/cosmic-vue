const SpotifyWebApi = require('spotify-web-api-node');
const { dbQuery } = require('./db');
const { debug } = require('./debug');
const { credentials } = require('../../config');

// Utils

const getTrackList = (data) => {
  const trackList = data.map((item) => {
    const newItem = {
      thumbnailUrl: item.track.album.images[0].url,

      name: item.track.name,

      trackUrl: item.track.external_urls.spotify,
      trackId: item.track.id,
      albumUrl: item.track.album.external_urls.spotify,
    };

    let reducedArtists = '';

    item.track.album.artists.forEach((artist, i, arr) => {
      reducedArtists = reducedArtists.concat(artist.name, i < arr.length - 1 ? ' & ' : '');
    });

    newItem.artists = reducedArtists;

    return newItem;
  });

  return trackList;
};

// API Handlers go here

exports.api = {
  blog: (req, res) => {
    dbQuery('SELECT * FROM Posts WHERE $1=ANY(tags)', ['blog'], (err, result) => {
      if (err) {
        debug('Error in db query', err.stack);
      } else {
        res.setHeader('Cache-Control', 'no-cache');
        res.json(result.rows);
      }
    });
  },

  music: {
    spotify: (req, res) => {
      const COSMIC_CUBE_PLAYLIST_ID = '3RjHBD69THydLiYqlCPVFm';
      const { clientId, clientSecret } = credentials.spotify;
      const spotifyApi = new SpotifyWebApi({ clientId, clientSecret });

      spotifyApi.clientCredentialsGrant().then(
        (data) => {
          spotifyApi.setAccessToken(data.body.access_token);
          spotifyApi.getPlaylistTracks(COSMIC_CUBE_PLAYLIST_ID).then(
            (payload) => {
              const tracks = getTrackList(payload.body.items);
              res.json(tracks);
            },
            (err) => {
              res.json(err);
            },
          );
        },
        (err) => {
          res.json(err);
        },
      );
    },

  },
};
