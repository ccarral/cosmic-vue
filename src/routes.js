import Blog from './components/Blog.vue';
import NotFound from './components/NotFound.vue';
import Landing from './components/Landing.vue';
import About from './components/About.vue';
import Music from './components/Music.vue';

const routes = [
  {
    path: '/',
    component: Landing,
  },
  {
    path: '/blog',
    component: Blog,
  },
  {
    path: '/about',
    component: About,
  },
  {
    path: '/music',
    component: Music,
  },
  {
    path: '*',
    component: NotFound,
  },

];

export default routes;
