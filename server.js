const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');
const handlers = require('./src/lib/handlers');
const { debug } = require('./src/lib/debug');

debug('Handlers', handlers);

const app = express();

app.set('port', (process.env.PORT || 3000));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use((req, res, next) => {
  res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
  res.setHeader('Pragma', 'no-cache');
  res.setHeader('Expires', '0');
  next();
});

app.get('/api/blog/posts', handlers.api.blog);

app.get('/api/music/spotify', handlers.api.music.spotify);

app.listen(app.get('port'), () => {
  console.log(`Find the server at: http://localhost:${app.get('port')}/`); // eslint-disable-line no-console
});
