import axios from 'axios';

const actions = {
  getSpotifyTracks: () => axios.get('/api/music/spotify'),
  getBlogPosts: () => axios.get('/api/blog/posts'),
};

export default actions;
