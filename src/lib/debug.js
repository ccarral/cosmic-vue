
const { env } = require('../../config');

exports.debug = (msg, ...args) =>{
    if (env === 'dev'){
      console.log(`=> ${msg}: ${args}`);
    }
}
